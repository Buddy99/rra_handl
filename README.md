# Handl_RRA_Project
Realtime Rendering and Algorithms Project - Anja Handl  

1. Projekt von Gitlab herunterladen 
2. .exe Datei starten oder Solution mit Visual Studio 2019 öffnen und kompilieren

Steuerung  
W, A, S, D - Bewegung  
Maus bewegen - Blickrichtung ändern  
Pfeiltaste rauf: Bumpiness erhöhen  
Pfeiltaste runter: Bumpiness verringern  
M: Anti-Aliasing an  
N: Anti-Aliasing aus  
2 / 4 / 8: Sample Mode ändern  
ENTER: Ray durch den Mittelpunkt des Fensters schießen  
B: KdTree Gitter anzeigen / ausblenden  
V: Würfel anzeigen / ausblenden  
G: Kamerafahrt Modus anschalten / ausschalten  
+: Geschwindigkeit der Kamera im Kamerafahrtmodus erhöhen  
-: Geschwindigkeit der Kamera im Kamerafahrtmodus verringern  
F: Neuen Kamerapunkt setzen  
C: Alle bisher gesetzten Kamerapunkte löschen  
