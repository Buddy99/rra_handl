#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>

class CameraPoint
{
public:
	CameraPoint();
	CameraPoint(glm::vec3 pos, glm::quat rot);

	glm::vec3 position;
	glm::quat rotation;
};
