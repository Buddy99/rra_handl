#pragma once
#define STB_IMAGE_IMPLEMENTATION

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>

#include <glBasics/shader.h>
#include <glBasics/camera.h>

#include <iostream>
#include <filesystem>
#include <chrono>
#include <vector>
#include "CameraPoint.h"
#include "KeyHandler.h"
#include "Cube.h"
#include "KdTree.h"


#define accuracy 0.01

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);
unsigned int loadTexture(const char* path);
void renderScene(Shader& shader);
void renderCube();
int reloadWindow();
int initialization();
void setupShadersTextures();
void renderLoop();
void onExit();
void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods);

// Camera Interpolation
void reset();
float doStep(float s);
CameraPoint interpolation(int currentPoint, float k);
void initializeCameraPoints();

// Settings
const unsigned int SCR_WIDTH = 1024;
const unsigned int SCR_HEIGHT = 768;

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = (float)SCR_WIDTH / 2.0;
float lastY = (float)SCR_HEIGHT / 2.0;
bool firstMouse = true;

// Light
glm::vec3 lightPos(-2.0f, 4.0f, -1.0f);

// Timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;
float currentFrame;

// BumpinessFactor
float bumpiness = 1.0f;

// Diffuse + Normal Maps
unsigned int diffuseMap;
unsigned int normalMap;
unsigned int diffuseMap2;
unsigned int normalMap2;
unsigned int diffuseMap3;
unsigned int normalMap3;
unsigned int diffuseMap4;
unsigned int normalMap4;

// Meshes
unsigned int planeVAO;
unsigned int planeVBO;
unsigned int cubeVAO = 0;
unsigned int cubeVBO = 0;

// Depth Map
const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
unsigned int depthMapFBO;
unsigned int depthMap;

// Anti-Aliasing
unsigned int multiSample = 2;

// Window
GLFWwindow* window;

// Shader
Shader* shader;
Shader* simpleDepthShader;

// Interpolation variables
float t = 0;	// Tension
float b = 0;	// Bias
float c = 0;	// Continuity
float d0a;
float d0b;
float d1a;
float d1b;

// Camera variables
bool movingCamera = false;
glm::vec3 cameraPosition;
glm::vec3 cameraRotation;
glm::vec3 mousePosition;
std::vector<CameraPoint> cameraPoints;
std::vector<float> distanceToNext;
std::vector<float> distanceToPoint;
float currentDistance = 0;
float currentCameraPoint = 0;
float speed = 10;

// KeyHandler
KeyHandler keyHandler;

// Cubes
std::vector<Cube*> myCubes;
Cube* hitCube;

// Ray + KdTree
Ray ray;
KdTree tree;

// KdTree Grid
Material boundingBoxMat;
bool drawBoundingBoxes = false;
bool drawAlbedo = true;
