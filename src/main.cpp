#include "main.h"

int main()
{
    if (initialization() == -1)
    {
        return -1;
    }

    setupShadersTextures();

    renderLoop();

    onExit();
    glfwTerminate();
    return 0;
}

int initialization()
{
    // Glfw: initialization and configuration
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, multiSample);

    #ifdef __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif

    // Glfw create window
    window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Handl_RRA", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Error: Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetKeyCallback(window, handleKey);

    // Mouse capturing
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Initiaize glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Error: Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // Configure global opengl state
    glEnable(GL_DEPTH_TEST);

    //initializeCameraPoints();
    d0a = ((1 - t) * (1 + c) * (1 + b)) / 2;
    d0b = ((1 - t) * (1 - c) * (1 - b)) / 2;
    d1a = ((1 - t) * (1 + c) * (1 - b)) / 2;
    d1b = ((1 - t) * (1 - c) * (1 + b)) / 2;
    lastFrame = glfwGetTime();
}

void setupShadersTextures()
{
    // Load shaders
    shader = new Shader();
    shader->load("vertexshader.vs", "fragmentshader.fs");
    simpleDepthShader = new Shader();
    simpleDepthShader->load("depth_vertexshader.vs", "depth_fragmentshader.fs");
    boundingBoxMat.UseShader(new Shader());
    boundingBoxMat.GetShader()->load("bv.vs", "bv.fs");

    // Floor plane
    float planeVertices[] = {
        // Positions            // Normals         // Texcoords
         25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
        -25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
        -25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

         25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
        -25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
         25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
    };
    // Plane VAO
    glGenVertexArrays(1, &planeVAO);
    glGenBuffers(1, &planeVBO);
    glBindVertexArray(planeVAO);
    glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glBindVertexArray(0);

    // Load textures and normal maps
    diffuseMap = loadTexture(std::filesystem::absolute("resources/textures/grass.jpg").string().c_str());
    normalMap = loadTexture(std::filesystem::absolute("resources/textures/grass_normal.jpg").string().c_str());

    // Cubes
    Cube* myCube = new Cube();
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.0f, 1.5f, 0.0));
    model = glm::scale(model, glm::vec3(0.5f));
    myCube->SetModelMatrix(model);
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greenrock.jpg").string().c_str()), GL_TEXTURE0));
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greenrock_normal.jpg").string().c_str()), GL_TEXTURE1));
    myCubes.push_back(myCube);

    myCube = new Cube();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(2.5f, 0.0f, 2.5));
    model = glm::scale(model, glm::vec3(0.5f));
    myCube->SetModelMatrix(model);
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greyrock.jpg").string().c_str()), GL_TEXTURE0));
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greyrock_normal.jpg").string().c_str()), GL_TEXTURE1));
    myCubes.push_back(myCube);

    myCube = new Cube();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(2.5f, 0.8f, 2.5));
    model = glm::scale(model, glm::vec3(0.4f));
    myCube->SetModelMatrix(model);
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greyrock.jpg").string().c_str()), GL_TEXTURE0));
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greyrock_normal.jpg").string().c_str()), GL_TEXTURE1));
    myCubes.push_back(myCube);

    myCube = new Cube();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(2.5f, 1.63f, 2.5));
    model = glm::rotate(model, glm::radians(45.0f), glm::normalize(glm::vec3(1.0, 0.0, 0.0)));
    model = glm::scale(model, glm::vec3(0.3f));
    myCube->SetModelMatrix(model);
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greyrock.jpg").string().c_str()), GL_TEXTURE0));
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/greyrock_normal.jpg").string().c_str()), GL_TEXTURE1));
    myCubes.push_back(myCube);

    myCube = new Cube();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(1.0f, 0.0f, -2.0));
    model = glm::rotate(model, glm::radians(60.0f), glm::normalize(glm::vec3(0.0, 0.0, 1.0)));
    model = glm::scale(model, glm::vec3(0.25));
    myCube->SetModelMatrix(model);
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/brownrock.jpg").string().c_str()), GL_TEXTURE0));
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/brownrock_normal.jpg").string().c_str()), GL_TEXTURE1));
    myCubes.push_back(myCube);

    myCube = new Cube();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(1.0f, 1.5f, -2.0));
    model = glm::rotate(model, glm::radians(60.0f), glm::normalize(glm::vec3(0.0, -1.0, 0.0)));
    model = glm::scale(model, glm::vec3(0.25));
    myCube->SetModelMatrix(model);
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/brownrock.jpg").string().c_str()), GL_TEXTURE0));
    myCube->GetMaterial()->mTextures.push_back(new Texture(loadTexture(std::filesystem::absolute("resources/textures/brownrock_normal.jpg").string().c_str()), GL_TEXTURE1));
    myCubes.push_back(myCube);

    // Configure depth map FBO
    glGenFramebuffers(1, &depthMapFBO);
    // Create depth texture
    glGenTextures(1, &depthMap);
    glBindTexture(GL_TEXTURE_2D, depthMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    // Attach depth texture as FBO's depth buffer
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Shader configuration
    shader->use();
    shader->setInt("diffuseMap", 0);
    shader->setInt("normalMap", 1);
    shader->setInt("shadowMap", 2);

    // Setup KdTree
    hitCube = nullptr;
    std::vector<Triangle*>* triangles = new std::vector<Triangle*>();
    triangles->reserve(myCubes.size()* Cube::MAX_TRIANGLES);
    for (auto node : myCubes)
    {
        Cube* cube = dynamic_cast<Cube*>(node);
        for (int i = 0; i < Cube::MAX_TRIANGLES; i++)
        {
            triangles->push_back(cube->GetTriangles()[i]);
        }
    }
    tree.BuildTree(triangles, 30);
    
}

void renderLoop()
{
    // Render loop
    while (!glfwWindowShouldClose(window))
    {
        // Calculate deltaTime
        currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // Handle input
        processInput(window);

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Render depth of scene to texture from light's perspective
        glm::mat4 lightProjection;
        glm::mat4 lightView;
        glm::mat4 lightSpaceMatrix;
        float near_plane = 1.0f, far_plane = 10.5f;
        lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
        lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
        lightSpaceMatrix = lightProjection * lightView;
        // Render scene from light's point of view
        simpleDepthShader->use();
        simpleDepthShader->setMat4("lightSpaceMatrix", lightSpaceMatrix);

        glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glClear(GL_DEPTH_BUFFER_BIT);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, diffuseMap);
        renderScene(*simpleDepthShader);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // Reset viewport
        glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Render scene using the generated depth/shadow map from the camera's perspective
        glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        glm::mat4 view = camera.GetViewMatrix();
        boundingBoxMat.use();
        boundingBoxMat.GetShader()->setMat4("uProjection", projection);
        boundingBoxMat.GetShader()->setMat4("uView", view);
        shader->use();
        shader->setMat4("projection", projection);
        shader->setMat4("view", view);

        // Set light uniforms
        shader->setVec3("viewPos", camera.Position);
        shader->setVec3("lightPos", lightPos);
        shader->setVec3("uColor", glm::vec3(1.0, 1.0, 1.0));
        shader->setMat4("lightSpaceMatrix", lightSpaceMatrix);
        shader->setFloat("bumpiness", bumpiness);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, depthMap);

        renderScene(*shader);

        // Glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        keyHandler.FrameUpdate();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}

void onExit()
{
    // De-allocation of resources
    delete shader;
    delete simpleDepthShader;
    glDeleteVertexArrays(1, &planeVAO);
    glDeleteBuffers(1, &planeVBO);
    glDeleteVertexArrays(1, &cubeVAO);
    glDeleteBuffers(1, &cubeVBO);
    planeVAO = 0;
    planeVBO = 0;
    cubeVAO = 0;
    cubeVBO = 0;
}

// Render objects with textures
void renderScene(Shader& shader)
{
    // Floor
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuseMap);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, normalMap);
    glm::mat4 model = glm::mat4(1.0f);
    shader.setMat4("model", model);
    glBindVertexArray(planeVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    if (drawAlbedo)
    {
        // Cubes
        for (auto cube : myCubes)
        {
            cube->GetMaterial()->UseSharedShader(&shader);
            cube->Draw();
        }
    }

    boundingBoxMat.use();
    ray.Draw();
    if (hitCube != nullptr)
    {
        hitCube->Draw();
    }
    if (drawBoundingBoxes)
    {
        glLineWidth(1);
        tree.DrawTree();
    }
    
}

// Render a 1x1 3D cube
void renderCube()
{
    // Initialize
    if (cubeVAO == 0)
    {
        float vertices[] = {
            // Positions         // Normals          // TexCoords
            // back face
            -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
             1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // Top-right
             1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // Bottom-right         
             1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // Top-right
            -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
            -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // Top-left
            // Front face
            -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // Bottom-left
             1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // Bottom-right
             1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // Top-right
             1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // Top-right
            -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // Top-left
            -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // Bottom-left
            // Left face
            -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // Top-right
            -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // Top-left
            -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // Bottom-left
            -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // Bottom-left
            -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // Bottom-right
            -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // Top-right
            // Right face
             1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // Top-left
             1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // Bottom-right
             1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // Top-right         
             1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // Bottom-right
             1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // Top-left
             1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // Bottom-left     
            // Bottom face
            -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // Top-right
             1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // Top-left
             1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // Bottom-left
             1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // Bottom-left
            -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // Bottom-right
            -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // Top-right
            // Top face
            -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // Top-left
             1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // Bottom-right
             1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // Top-right     
             1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // Bottom-right
            -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // Top-left
            -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // Bottom-left        
        };
        glGenVertexArrays(1, &cubeVAO);
        glGenBuffers(1, &cubeVBO);
        // Fill buffer
        glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        // Link vertex attributes
        glBindVertexArray(cubeVAO);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }
    // Render Cube
    glBindVertexArray(cubeVAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
}

// Load the whole scene again and recreate the window
int reloadWindow()
{
    onExit();
    glfwTerminate();

    if (initialization() == -1)
    {
        return -1;
    }

    setupShadersTextures();
}

// Handle Key input
void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (action)
    {
    case GLFW_PRESS:
        keyHandler.KeyPressed(key);
        break;

    case GLFW_RELEASE:
        keyHandler.KeyReleased(key);
        break;

    default:
        break;
    }
}

// Process all input (pressed keys, etc.)
void processInput(GLFWwindow* window)
{
    // Exit
    if (keyHandler.WasKeyReleased(GLFW_KEY_ESCAPE))
    {
        glfwSetWindowShouldClose(window, true);
    }
    
    if (!movingCamera)
    {
        // Set Camera Points Mode

        // Movement
        if (keyHandler.IsKeyDown(GLFW_KEY_W))
        {
            camera.ProcessKeyboard(Camera::Camera_Movement::FORWARD, deltaTime);
        }
        if (keyHandler.IsKeyDown(GLFW_KEY_S))
        {
            camera.ProcessKeyboard(Camera::Camera_Movement::BACKWARD, deltaTime);
        }
        if (keyHandler.IsKeyDown(GLFW_KEY_A))
        {
            camera.ProcessKeyboard(Camera::Camera_Movement::LEFT, deltaTime);
        }
        if (keyHandler.IsKeyDown(GLFW_KEY_D))
        {
            camera.ProcessKeyboard(Camera::Camera_Movement::RIGHT, deltaTime);
        }

        // Set spline points 
        if (keyHandler.WasKeyReleased(GLFW_KEY_F))
        {
            cameraPoints.push_back({ { camera.Position }, camera.Rotation });
        }

        // Clear all spline points
        if (keyHandler.WasKeyReleased(GLFW_KEY_C))
        {
            cameraPoints.clear();
        }

        // Shoot Ray
        if (keyHandler.WasKeyReleased(GLFW_KEY_ENTER))
        {
            ray = Ray(camera.Position, camera.Front);
            ray.UpdateLine();
            if (tree.mLastHit != nullptr)
            {
                tree.mLastHit->mIsHit = false;
            }
            tree.CheckIntersect(ray);
            if (tree.mLastHit != nullptr)
            {
                tree.mLastHit->mIsHit = true;
                ray.mHitDistance = tree.mHitDistance;
                std::cout << "Distance: " << tree.mHitDistance << std::endl;
                std::cout << "HitPosition: " << tree.mLastHitPosition.x << ", " << tree.mLastHitPosition.y << ", " << tree.mLastHitPosition.z << std::endl;
                Cube* t = new Cube();
                t->SetMaterial(&boundingBoxMat, false);
                glm::mat4 model(1.0f);
                model = glm::translate(model, tree.mLastHitPosition);
                model = glm::scale(model, glm::vec3(0.02f));
                t->SetModelMatrix(model);
                std::swap(t, hitCube);
                delete t;
            }
        }

        // Enable / Disable KdTree grid
        if (keyHandler.WasKeyReleased(GLFW_KEY_B))
        {
            drawBoundingBoxes = !drawBoundingBoxes;
        }

        // Enable / disable Cube drawing
        if (keyHandler.WasKeyPressed(GLFW_KEY_V))
        {
            drawAlbedo = !drawAlbedo;
        }
    }
    else
    {
        // Moving Camera Mode

        // Camera Spline
        float k = doStep(deltaTime * speed);
        CameraPoint myCameraPoint = interpolation((int)k, fmodf(k, 1.0));
        currentCameraPoint++;
        camera.Position.x = myCameraPoint.position.x;
        camera.Position.y = myCameraPoint.position.y;
        camera.Position.z = myCameraPoint.position.z;
        camera.UpdateRotation(myCameraPoint.rotation);
    }
    
    // Switch between camera moving and point setting modes
    if (keyHandler.WasKeyReleased(GLFW_KEY_G))
    {
        if (movingCamera)
        {
            movingCamera = false;
        }
        else
        {
            movingCamera = true;
            reset();
        }
        lastFrame = glfwGetTime();
    }

    // Change speed
    if (keyHandler.WasKeyReleased(GLFW_KEY_KP_ADD))
    {
        speed += 10;
    }
    if (keyHandler.WasKeyReleased(GLFW_KEY_KP_SUBTRACT))
    {
        speed -= 10;
    }

    // Increase bumpiness
    if (keyHandler.IsKeyDown(GLFW_KEY_UP))
    {
        bumpiness += deltaTime;
        bumpiness = glm::clamp(bumpiness, 0.0f, 1.0f);
    }
    // Decrease bumpiness
    if (keyHandler.IsKeyDown(GLFW_KEY_DOWN))
    {
        bumpiness -= deltaTime;
        bumpiness = glm::clamp(bumpiness, 0.0f, 1.0f);
    }

    // Enable Anti-Aliasing
    if (keyHandler.WasKeyReleased(GLFW_KEY_M))
    {
        glEnable(GL_MULTISAMPLE);
    }
    if (keyHandler.WasKeyReleased(GLFW_KEY_N))
    {
        glDisable(GL_MULTISAMPLE);
    }

    // Set Anti-Aliasing Mode
    if (keyHandler.WasKeyReleased(GLFW_KEY_2))
    {
        if (multiSample != 2)
        {
            multiSample = 2;
            reloadWindow();
        } 
    }
    if (keyHandler.WasKeyReleased(GLFW_KEY_4))
    {
        if (multiSample != 4)
        {
            multiSample = 4;
            reloadWindow();
        }
    }
    if (keyHandler.WasKeyReleased(GLFW_KEY_8))
    {
        if (multiSample != 8)
        {
            multiSample = 8;
            reloadWindow();
        }
    }
}

// Handle window size changes
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

// Handle mouse movement
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// Handle mouse scrolling
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}

// Load a 2D texture from a file
unsigned int loadTexture(char const* path)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Error: Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}

void initializeCameraPoints()
{
    //Camera Points

    cameraPoints.push_back({ { 0, 2, 0 }, glm::quat(0, glm::vec3{ 1,1,1 }) });
    cameraPoints.push_back({ { -5, 4, -5 }, glm::quat(45, glm::vec3{ 0,1,0 }) });
    cameraPoints.push_back({ { -10, 6, -10 }, glm::quat(90, glm::vec3{ 0,1,0 }) });
    cameraPoints.push_back({ { -15, 8, -15 }, glm::quat(135, glm::vec3{ 0,1,0 }) });
    cameraPoints.push_back({ { -20, 10, -20 }, glm::quat(180, glm::vec3{ 0,1,0 }) });

    cameraPoints.push_back({ { -15, 12, -25 }, glm::quat(135, glm::vec3{ 0,1,0 }) });
    cameraPoints.push_back({ { -10, 14, -30 }, glm::quat(90, glm::vec3{ 0,1,0 }) });
    cameraPoints.push_back({ { -5, 16, -35 }, glm::quat(45, glm::vec3{ 0,1,0 }) });
    cameraPoints.push_back({ { 0, 18, -40 }, glm::quat(0, glm::vec3{ 0,1,0 }) });
    cameraPoints.push_back({ { 5, 16, -35 }, glm::quat(-45, glm::vec3{ 1,0,0 }) });

    cameraPoints.push_back({ { 10, 14, -30 }, glm::quat(-90, glm::vec3{ 1,0,0 }) });
    cameraPoints.push_back({ { 15, 12, -25 }, glm::quat(-135, glm::vec3{ 1,0,0 }) });
    cameraPoints.push_back({ { 20, 10, -20 }, glm::quat(-180, glm::vec3{ 1,0,0 }) });
    cameraPoints.push_back({ { 15, 8, -15 }, glm::quat(-135, glm::vec3{ 1,0,0 }) });
    cameraPoints.push_back({ { 10, 6, -10 }, glm::quat(-90, glm::vec3{ 1,0,0 }) });

    cameraPoints.push_back({ { 5, 4, -5 }, glm::quat(-45, glm::vec3{ 1,0,0 }) });
    cameraPoints.push_back({ { 0, 2, 0 }, glm::quat(0, glm::vec3{ 1,0,0 }) });
    cameraPoints.push_back({ { -5, 4, 5 }, glm::quat(45, glm::vec3{ 0,0,1 }) });
    cameraPoints.push_back({ { -10, 6, 10 }, glm::quat(90, glm::vec3{ 0,0,1 }) });
    cameraPoints.push_back({ { -15, 8, 15 }, glm::quat(135, glm::vec3{ 0,0,1 }) });
    cameraPoints.push_back({ { -20, 10, 20 }, glm::quat(180, glm::vec3{ 0,0,1 }) });

    cameraPoints.push_back({ { -15, 12, 25 }, glm::quat(135, glm::vec3{ 0,0,1 }) });
    cameraPoints.push_back({ { -10, 14, 30 }, glm::quat(90, glm::vec3{ 0,0,1 }) });
    cameraPoints.push_back({ { -5, 16, 35 }, glm::quat(45, glm::vec3{ 0,0,1 }) });
    cameraPoints.push_back({ { 0, 18, 40 }, glm::quat(0, glm::vec3{ 0,0,1 }) });
    cameraPoints.push_back({ { 5, 16, 35 }, glm::quat(45, glm::vec3{ 1,1,1 }) });

    cameraPoints.push_back({ { 10, 14, 30 }, glm::quat(90, glm::vec3{ 1,1,1 }) });
    cameraPoints.push_back({ { 15, 12, 25 }, glm::quat(135, glm::vec3{ 1,1,1 }) });
    cameraPoints.push_back({ { 20, 10, 20 }, glm::quat(180, glm::vec3{ 1,1,1 }) });
    cameraPoints.push_back({ { 15, 8, 15 }, glm::quat(135, glm::vec3{ 1,1,1 }) });
    cameraPoints.push_back({ { 10, 6, 10 }, glm::quat(90, glm::vec3{ 1,1,1 }) });

    cameraPoints.push_back({ { 5, 4, 5 }, glm::quat(45, glm::vec3{ 1,1,1 }) });
    cameraPoints.push_back({ { 0, 2, 0 }, glm::quat(0, glm::vec3{ 1,1,1 }) });
}

CameraPoint interpolation(int currentPoint, float k)
{
    if (cameraPoints.size() > 0)
    {
        glm::quat quatSlerp;
        glm::quat quatSquad;

        glm::quat rot;
        glm::quat nextRot;
        glm::quat nextNextRot;
        glm::quat prevRot;

        glm::quat intermedPoint1;
        glm::quat intermedPoint2;

        glm::vec3 pos;
        glm::vec3 point;
        glm::vec3 prevPoint;
        glm::vec3 nextPoint;
        glm::vec3 nextNextPoint;

        glm::vec3 d0;
        glm::vec3 d1;

        int numberOfPoints = cameraPoints.size() - 1;

        // Handle edge cases
        point = cameraPoints[std::clamp(currentPoint, 0, numberOfPoints)].position;
        prevPoint = cameraPoints[std::clamp(currentPoint - 1, 0, numberOfPoints)].position;
        nextPoint = cameraPoints[std::clamp(currentPoint + 1, 0, numberOfPoints)].position;
        nextNextPoint = cameraPoints[std::clamp(currentPoint + 2, 0, numberOfPoints)].position;

        rot = cameraPoints[std::clamp(currentPoint, 0, numberOfPoints)].rotation;
        prevRot = cameraPoints[std::clamp(currentPoint - 1, 0, numberOfPoints)].rotation;
        nextRot = cameraPoints[std::clamp(currentPoint + 1, 0, numberOfPoints)].rotation;
        nextNextRot = cameraPoints[std::clamp(currentPoint + 2, 0, numberOfPoints)].rotation;

        // Kochanek-Bartels
        d0 = d0a * (point - prevPoint) + d0b * (nextPoint - point);
        d1 = d1a * (nextPoint - point) + d1b * (nextNextPoint - nextPoint);

        float ax, bx, cx, dx;
        float ay, by, cy, dy;
        float az, bz, cz, dz;
        // For x
        ax = 2.0 * point.x - 2.0 * nextPoint.x + d0.x + d1.x;
        bx = -3.0 * point.x + 3.0 * nextPoint.x - 2.0 * d0.x - d1.x;
        cx = d0.x;
        dx = point.x;
        // For y
        ay = 2.0 * point.y - 2.0 * nextPoint.y + d0.y + d1.y;
        by = -3.0 * point.y + 3.0 * nextPoint.y - 2.0 * d0.y - d1.y;
        cy = d0.y;
        dy = point.y;
        // For z
        az = 2.0 * point.z - 2.0 * nextPoint.z + d0.z + d1.z;
        bz = -3.0 * point.z + 3.0 * nextPoint.z - 2.0 * d0.z - d1.z;
        cz = d0.z;
        dz = point.z;

        float time2 = 0, time3 = 0;

        if (k != 0)
        {
            time2 = k * k;
            time3 = time2 * k;
        }
        intermedPoint1 = glm::intermediate(prevRot, rot, nextRot);
        intermedPoint2 = glm::intermediate(rot, nextRot, nextNextRot);
        quatSquad = glm::squad(rot, nextRot, intermedPoint1, intermedPoint2, k);
        pos = { ax * time3 + bx * time2 + cx * k + dx, ay * time3 + by * time2 + cy * k + dy, az * time3 + bz * time2 + cz * k + dz };
        return CameraPoint(pos, quatSquad);
    }
    return CameraPoint({ 0,0,0 }, { 0, {0, 0 ,0} });
}

void reset()
{
    currentCameraPoint = 0;
    currentDistance = 0;
    distanceToPoint.clear();
    distanceToNext.clear();

    if (cameraPoints.size() == 0)
    {
        return;
    }

    // Calculate distances for constant speed
    for (int i = 0; i < static_cast<int>(cameraPoints.size()); i++)
    {
        distanceToPoint.push_back(0);
        distanceToNext.push_back(0);
    }
    glm::vec3 previousP = cameraPoints[0].position;
    for (float k = accuracy; k <= cameraPoints.size() - 1; k += accuracy)
    {
        glm::vec3 currentP = interpolation((int)k, fmodf(k, 1.0)).position;
        distanceToNext[static_cast<int>(k - accuracy)] += glm::distance(previousP, currentP);
        previousP = currentP;
    }
    for (int i = 1; i < static_cast<int>(cameraPoints.size()); i++)
    {
        for (int j = 0; j < i; j++)
        {
            distanceToPoint[i] += distanceToNext[j];
        }
    }
}

float doStep(float s)
{
    // Calculate distances for constant speed
    int index = -1;
    currentDistance += s;
    for (int i = static_cast<int>(currentCameraPoint); i < static_cast<int>(distanceToPoint.size()); i++)
    {
        if (currentDistance <= distanceToPoint[i])
        {
            index = i - 1;
            break;
        }
    }
    if (index == -1)
    {
        currentCameraPoint = cameraPoints.size() + 1;
    }
    else
    {
        float dist = currentDistance - distanceToPoint[index];
        float splinePart = dist / distanceToNext[index];
        currentCameraPoint = index + splinePart;
    }
    return currentCameraPoint;
}
